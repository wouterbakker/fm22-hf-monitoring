@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>About</h1>
@stop

@section('content')
<div style="text-align: center;">
    This software is written by Wouter Bakker (wouter.bakker@rdi.nl) and licensed under European Union Public Licence Version 1.2 (EUPLv1.2). <br> If you have any questions about this software please contact me by email.
    The source code of this software is published on GitLab. <a href="https://gitlab.com/wouterbakker/fm22-hf-monitoring">https://gitlab.com/wouterbakker/fm22-hf-monitoring</a>. Feel free to create an issue or merge request there. <br>
    <br>
    The measurement schedule can be downloaded as csv or excel format.<br>
    <a href="schedule/FM_22_HF_schedule_2022_2026.csv">CSV download</a> <br>
    <a href="schedule/FM_22_HF_schedule_2022_2026.xlsx">Excel download</a> <br>
</div>

@stop
@section('css')

@stop

@section('js')

@stop
