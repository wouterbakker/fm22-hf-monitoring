@extends('adminlte::page')

@section('title', 'Registrations FM22 HF monitoring')

@section('content_header')
    <h1>Registrations</h1>
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h4>Frequency selection</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <label for="startFreq" class="col-2 col-form-label">Start <br>(kHz)</label>
                <input class="form-control" type="number" value="1" id="startFreq">
            </div>
            <div class="col-md-3">
                <label for="endFreq" class="col-2 col-form-label">End <br>(kHz)</label>
                <input class="form-control" type="number" value="10000000000" id="endFreq">
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <h4>Site selection</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <select id='selSite' multiple="multiple" style="width: 258px;">
                    <option value='0'>-- Select site --</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <h4>Date selection</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <input class="date form-control"  type="text" id="datepickerStart" name="date" placeholder="Select start date">
            </div>
            <div class="col-md-3">
                <input class="date form-control"  type="text" id="datepickerEnd" name="date" placeholder="Select end date">
            </div>
            <div class="col-md-4">
                <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
                <button type="button" name="refresh" id="refresh" class="btn btn-default">Refresh</button>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="box">

                    <div class="box-header">
                        @if(Session::has('message'))
                            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                        @endif
                    </div>
                    <div class="box-body">
                        <table id="registrations" class="table table-bordered table-hover dataTable">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Start frequency (kHz)</th>
                                <th>End frequency (kHz)</th>
                                <th>Site Name</th>
                                <th>Date</th>

                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
    </div>
@stop

@section('css')

    <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.standalone.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css" rel="stylesheet">

@stop

@section('js')
    {{--    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>--}}
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.min.js"></script>
    <script type="text/javascript">
        $('#datepickerStart').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd'
        });
    </script>

    <script>
        // CSRF Token
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready( function () {
            $('.js-example-basic-multiple').select2();

            load_data();

            function load_data(from_date = '', to_date = '', sites = '',startFreq = '',endFreq = '') {

                $("#startFreq").on('change', function () {
                    var from_date = $('#datepickerStart').val();
                    var to_date = $('#datepickerEnd').val();
                    var sites = $('#selSite').select2("val");
                    var startFreq = $('#startFreq').val();
                    var endFreq = $('#endFreq').val();

                    $('#registrations').DataTable().destroy();
                    load_data(from_date,to_date,sites,startFreq,endFreq);
                });
                $("#endFreq").on('change', function () {
                    var from_date = $('#datepickerStart').val();
                    var to_date = $('#datepickerEnd').val();
                    var sites = $('#selSite').select2("val");
                    var startFreq = $('#startFreq').val();
                    var endFreq = $('#endFreq').val();

                    $('#registrations').DataTable().destroy();
                    load_data(from_date,to_date,sites,startFreq,endFreq);
                });
                $('#registrations').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: '{{ route("registrations.index") }}',
                        data: {from_date: from_date, to_date: to_date, sites: sites, startFreq: startFreq, endFreq:endFreq}
                    },
                    columns: [
                        {data: 'id', name: 'id', render: function(data, type, full, meta) {
                                return '<a href="showregistration/'+data+'">Show registration '+ data + '</a>'}},
                        {data: 'startFreq', name: 'startFreq'},
                        {data: 'endFreq', name: 'endFreq'},
                        {data: 'site', name: 'site.name'},
                        {data: 'measurementDate', name: 'measurementDate'},
                    ]
                });
            }
            $('#filter').click(function(){
                var from_date = $('#datepickerStart').val();
                var to_date = $('#datepickerEnd').val();
                var sites = $('#selSite').select2("val");
                var startFreq = $('#startFreq').val();
                var endFreq = $('#endFreq').val();

                $('#registrations').DataTable().destroy();
                load_data(from_date,to_date,sites,startFreq,endFreq);

            });

            $('#refresh').click(function(){
                $('#datepickerStart').datepicker('setDate', null);
                $('#datepickerEnd').datepicker('setDate', null);
                $('#selSite').val(null).trigger('change');
                $('#startFreq').val('1');
                $('#endFreq').val('10000000000');
                $('#registrations').DataTable().destroy();
                load_data();
            });
            $( "#selSite" ).select2({

                ajax: {
                    url: "{{ route('registrations.getSites') }}",
                    type: "post",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            _token: CSRF_TOKEN,
                            search: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true

                },
                placeholder: 'Search for a measurement site',

            });
            $('#selSite').on("select2:select", function(e) {
                var from_date = $('#datepickerStart').val();
                var to_date = $('#datepickerEnd').val();
                var sites = $('#selSite').select2("val");
                var startFreq = $('#startFreq').val();
                var endFreq = $('#endFreq').val();

                $('#registrations').DataTable().destroy();
                load_data(from_date,to_date,sites,startFreq,endFreq);
            });
            $('#selSite').on("select2:unselect", function(e) {
                var from_date = $('#datepickerStart').val();
                var to_date = $('#datepickerEnd').val();
                var sites = $('#selSite').select2("val");
                var startFreq = $('#startFreq').val();
                var endFreq = $('#endFreq').val();

                $('#registrations').DataTable().destroy();
                load_data(from_date,to_date,sites,startFreq,endFreq);
            });


            $('#datepickerEnd').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
            }).on('changeDate',function(e) {
                var from_date = $('#datepickerStart').val();
                var to_date = $('#datepickerEnd').val();
                var sites = $('#selSite').select2("val");
                var startFreq = $('#startFreq').val();
                var endFreq = $('#endFreq').val();

                $('#registrations').DataTable().destroy();
                load_data(from_date,to_date,sites,startFreq,endFreq);
            });
            $('#datepickerStart').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
            }).on('changeDate',function(e) {
                var from_date = $('#datepickerStart').val();
                var to_date = $('#datepickerEnd').val();
                var sites = $('#selSite').select2("val");
                var startFreq = $('#startFreq').val();
                var endFreq = $('#endFreq').val();

                $('#registrations').DataTable().destroy();
                load_data(from_date,to_date,sites,startFreq,endFreq);
            });

            $(document).on('keypress',function(e) {
                if(e.which == 13) {
                    var from_date = $('#datepickerStart').val();
                    var to_date = $('#datepickerEnd').val();
                    var sites = $('#selSite').select2("val");
                    var startFreq = $('#startFreq').val();
                    var endFreq = $('#endFreq').val();

                    $('#registrations').DataTable().destroy();
                    load_data(from_date,to_date,sites,startFreq,endFreq);
                }
            });


        });


    </script>
@stop
