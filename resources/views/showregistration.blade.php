@extends('adminlte::page')

@section('title', 'Show registration')

@section('content_header')
    <div style="text-align: center;">
        <h1>{{$dateString}} </h1>
        <h1>{{$siteName}}</h1>
        <h1>{{$startFreq}} kHz - {{$endFreq}} kHz</h1>
    </div>

@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div style="text-align: center;">
                    <h3> Spectrogram </h3>
                </div>
                <img src="{{ asset($spectrogramUrl)}}" alt="Spectrogram" width="100%">
            </div>
            <div class="col-xl-12">
                <div style="text-align: center;">
                    <h3> Bandplan </h3>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box">

                        <div class="box-header">
                            @if(Session::has('message'))
                                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                            @endif
                        </div>
                        <div class="box-body">
                            <table id="bandplan" class="table table-bordered table-hover dataTable">
                                <thead>
                                <tr>
                                    <th>Start frequency (kHz)</th>
                                    <th>End frequency (kHz)</th>
                                    <th>RR Region 1 Allocation</th>
                                    <th>Application</th>
                                    <th>Standard</th>
                                    <th>Notes</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <div class="col-xl-6">
                <div style="text-align: center;">
                    <h3> Spectra </h3>
                </div>
                <img src="{{ asset($spectraUrl)}}" alt="Spectrogram" width="100%">
            </div>
            <div class="col-xl-6">
                <div style="text-align: center;">
                    <h3> Occupancy </h3>
                </div>
                <img src="{{ asset($occupancyUrl)}}" alt="Waterfall" width="100%">
            </div>
            <div class="col-xl-6">
                <div style="text-align: center;">
                    <h3> Waterfall </h3>
                </div>
                <img src="{{ asset($waterfallUrl)}}" alt="Waterfall" width="100%">
            </div>
        </div>
    </div>

@stop

@section('css')
    <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
@stop

@section('js')
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready( function () {

            load_data("{{$startFreq}}" ,"{{$endFreq}}" );

            function load_data(startFreq = '' ,endFreq =  '') {
                $('#bandplan').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: '{{ route("bandplan.index") }}',
                        data: {startFreq: startFreq, endFreq:endFreq}
                    },
                    columns: [
                        {data: 'startFreq', name: 'startFreq'},
                        {data: 'endFreq', name: 'endFreq'},
                        {data: 'allocation', name: 'allocation'},
                        {data: 'applications', name: 'applications'},
                        {data: 'standard', name: 'standard'},
                        {data: 'notes', name: 'notes'},
                    ]
                });
            }
        });

    </script>
@stop
