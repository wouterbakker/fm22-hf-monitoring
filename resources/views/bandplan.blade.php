@extends('adminlte::page')

@section('title', 'Bandplan')

@section('content_header')
    <h1>Bandplan</h1>
@stop

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-3">
                <h4>Frequency selection</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <label for="startFreq" class="col-2 col-form-label">Start <br>(kHz)</label>
                <input class="form-control" type="number" value="1" id="startFreq">
            </div>
            <div class="col-md-3">
                <label for="endFreq" class="col-2 col-form-label">End <br>(kHz)</label>
                <input class="form-control" type="number" value="10000000000" id="endFreq">
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-4">
                <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
                <button type="button" name="refresh" id="refresh" class="btn btn-default">Refresh</button>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="box">

                    <div class="box-header">
                        @if(Session::has('message'))
                            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                        @endif
                    </div>
                    <div class="box-body">
                        <table id="bandplan" class="table table-bordered table-hover dataTable">
                            <thead>
                            <tr>
                                <th>Start frequency (kHz)</th>
                                <th>End frequency (kHz)</th>
                                <th>RR Region 1 Allocation</th>
                                <th>Application</th>
                                <th>Standard</th>
                                <th>Notes</th>

                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <div class="row">
            <div class="col-md-12">
                <div style="text-align: center;">
                    Source: <a href="https://www.efis.dk/sitecontent.jsp?sitecontent=ecatable" target="_blank">https://www.efis.dk/sitecontent.jsp?sitecontent=ecatable</a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')

    <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">

@stop

@section('js')
    {{--    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>--}}
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready( function () {
            load_data();

            function load_data(startFreq = '',endFreq = '') {
                $('#bandplan').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: '{{ route("bandplan.index") }}',
                        data: {startFreq: startFreq, endFreq:endFreq}
                    },
                    columns: [
                        {data: 'startFreq', name: 'startFreq'},
                        {data: 'endFreq', name: 'endFreq'},
                        {data: 'allocation', name: 'allocation'},
                        {data: 'applications', name: 'applications'},
                        {data: 'standard', name: 'standard'},
                        {data: 'notes', name: 'notes'},
                    ]
                });
            }
            $('#filter').click(function(){

                var startFreq = $('#startFreq').val();
                var endFreq = $('#endFreq').val();

                $('#bandplan').DataTable().destroy();
                load_data(startFreq,endFreq);

            });

            $('#refresh').click(function(){
                $('#startFreq').val('1');
                $('#endFreq').val('10000000000');
                $('#bandplan').DataTable().destroy();
                load_data();
            });
            $(document).on('keypress',function(e) {
                if(e.which == 13) {
                    var startFreq = $('#startFreq').val();
                    var endFreq = $('#endFreq').val();

                    $('#bandplan').DataTable().destroy();
                    load_data(startFreq,endFreq);
                }
            });



        });

    </script>
@stop
