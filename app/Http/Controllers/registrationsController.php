<?php

namespace App\Http\Controllers;

use App\sites;
use App\registrations;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;

class registrationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return
     */

    public function index(Request $request)
    {
        if(request()->ajax()) {
            $registrations = registrations::with('site');

            if (!empty($request->from_date)){
                $registrations->where('measurementDate','>=', $request->from_date);
            }
            if (!empty($request->to_date)){
                $registrations->where('measurementDate','<=', $request->to_date);
            }
            if (!empty($request->sites)){
                $registrations->whereIN('siteID', $request->sites);
            }
            if (!empty($request->startFreq)){
                $registrations->where('endFreq','>',($request->startFreq));
            }
            if (!empty($request->endFreq)){
                $registrations->where('startFreq','<',($request->endFreq));
            }

            return (new DataTables)->eloquent($registrations)->addColumn('site', function (registrations $registration) {
                return $registration->site->name;
            })->toJson();
        }
        return view('registrations');
    }
    public function getSites(Request $request){

        $search = $request->search;

        if($search == ''){
            $sites = sites::orderby('name','asc')->select('id','name')->limit(40)->get();
        }else{
            $sites = sites::orderby('name','asc')->select('id','name')->where('name', 'like', '%' .$search . '%')->limit(40)->get();
        }

        $response = array();
        foreach($sites as $site){
            $response[] = array(
                "id"=>$site->id,
                "text"=>$site->name
            );
        }

        echo json_encode($response);
        exit;
    }


}
