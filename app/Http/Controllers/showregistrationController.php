<?php

namespace App\Http\Controllers;

use App\sites;
use Illuminate\Http\Request;
use App\registrations;

class showregistrationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke($id)
    {
        $startFreq = registrations::where('id',$id)->value('startFreq');
        $endFreq = registrations::where('id',$id)->value('endFreq');
        $dateString = registrations::where('id',$id)->value('measurementDate');
        $date = strtotime($dateString);
        $siteID = sprintf('%02d', registrations::where('id',$id)->value('siteId'));
        $siteName = sites::where('id',$siteID)->value('name');

        $day = date('d', $date);
        $month = date('m', $date);
        $year = date('y', $date);

        $spectrogramUrl =
            'data/' . $startFreq . '_'. $endFreq . '_' . $day . $month . $year .'/' .
            $startFreq . "_" . $endFreq . '_' . $day . $month . $year . '_' . $siteID . '_speco.png';
        $waterfallUrl =
            'data/' . $startFreq . '_'. $endFreq . '_' . $day . $month . $year .'/' .
            $startFreq . "_" . $endFreq . '_' . $day . $month . $year . '_' . $siteID . '_water.png';
        $spectraUrl =
            'data/' . $startFreq . '_'. $endFreq . '_' . $day . $month . $year .'/' .
            $startFreq . "_" . $endFreq . '_' . $day . $month . $year . '_' . $siteID . '_speca.png';
        $occupancyUrl =
            'data/' . $startFreq . '_'. $endFreq . '_' . $day . $month . $year .'/' .
            $startFreq . "_" . $endFreq . '_' . $day . $month . $year . '_' . $siteID . '_occup.png';

        return view('showregistration', [
            'dateString' => $dateString,
            'siteName' => $siteName,
            'startFreq' => $startFreq,
            'endFreq'=> $endFreq,
            'spectrogramUrl' => $spectrogramUrl,
            'waterfallUrl' => $waterfallUrl,
            'spectraUrl' => $spectraUrl,
            'occupancyUrl' => $occupancyUrl
        ]);
    }
}
