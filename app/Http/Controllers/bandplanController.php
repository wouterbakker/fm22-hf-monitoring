<?php

namespace App\Http\Controllers;

use App\bandplan;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;

class bandplanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return
     */

    public function index(Request $request)
    {
        if(request()->ajax()) {
            $bandplan = bandplan::select('startFreq','endFreq','allocation','applications','standard','notes');

            if (!empty($request->startFreq)){
                $bandplan->where('endFreq','>',($request->startFreq));
            }
            if (!empty($request->endFreq)){
                $bandplan->where('startFreq','<',($request->endFreq));
            }

            return (new DataTables)->eloquent($bandplan)->toJson();
        }
        return view('bandplan');
    }

}
