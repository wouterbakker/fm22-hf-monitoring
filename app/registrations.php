<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class registrations extends Model
{
    protected $fillable = [
        'startFreq', 'endFreq','measurementDate','siteID'
    ];

    public function site()
    {
        return $this->belongsTo(sites::class,'siteID', );
    }
}
