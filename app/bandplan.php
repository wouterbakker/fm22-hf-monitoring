<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bandplan extends Model
{
    protected $fillable = [
        'startFreq', 'endFreq','allocation','region1FrequencyRangeFootnotes','ECAFootnotes','ECAFrequencyRangeFootnotes','harmonisationMeasure','applications','standard','notes'
    ];
}
