<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sites extends Model
{
    protected $fillable = [
        'name', 'country'
    ];

    public function registration()
    {
        return $this->hasMany(registrations::class,'id', 'siteID');
    }
}
