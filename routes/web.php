<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('registrations');
});
Route::get('about', function () {
    return view('about');
});
Route::resource('registrations', 'registrationsController');
Route::post('/registrations/getSites/','registrationsController@getSites')->name('registrations.getSites');
Route::get('showregistration/{id}', 'showregistrationController');
Route::resource('bandplan', 'bandplanController');

