<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBandplansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bandplans', function (Blueprint $table) {
            $table->id();
            $table->double('startFreq',64,2);
            $table->double('endFreq',64,2);
            $table->string('allocation');
            $table->string('region1FrequencyRangeFootnotes');
            $table->string('ECAFootnotes');
            $table->string('ECAFrequencyRangeFootnotes');
            $table->string('harmonisationMeasure');
            $table->string('applications');
            $table->string('standard');
            $table->longText('notes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bandplans');
    }
}
